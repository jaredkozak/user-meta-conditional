<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://kozan.dev
 * @since             1.0.0
 * @package           User_Meta_Conditional
 *
 * @wordpress-plugin
 * Plugin Name:       User Meta Conditional
 * Plugin URI:        https://kozan.dev/wordpress/user-meta-conditional
 * Description:       Add conditional content sections based on user meta and release times
 * Version:           1.0.1
 * Author:            Jared Kozak
 * Author URI:        https://kozan.dev
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       user-meta-conditional
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'USER_META_CONDITIONAL_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-user-meta-conditional-activator.php
 */
function activate_user_meta_conditional() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-user-meta-conditional-activator.php';
	User_Meta_Conditional_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-user-meta-conditional-deactivator.php
 */
function deactivate_user_meta_conditional() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-user-meta-conditional-deactivator.php';
	User_Meta_Conditional_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_user_meta_conditional' );
register_deactivation_hook( __FILE__, 'deactivate_user_meta_conditional' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-user-meta-conditional.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_user_meta_conditional() {

	$plugin = new User_Meta_Conditional();
	$plugin->run();

}
run_user_meta_conditional();
