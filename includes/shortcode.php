<?php

function display_current_time($atts) {
    $format = 'r';
    if (array_key_exists('format')) {
        $format = $atts['format'];
    }

    $tz = "America/Winnipeg";
    if (array_key_exists('tz', $atts)) {
        $tz = $atts['tz'];
    }

    $dt = new DateTime("now", new DateTimeZone($tz));

    return $dt->format($format);
}

function get_page_user_id() {

    $admin = current_user_can( 'manage_options' );
    $user_id = get_current_user_id();
    if ($admin && array_key_exists('user_id', $_GET)) {
        $user_id = $_GET['user_id'];
    }

    return $user_id;
}

function display_meta_value($atts) {
    $user_id = get_page_user_id();
    $defaultVal = NULL;
    if (array_key_exists('default', $atts)) {
        $defaultVal = htmlspecialchars($atts['default']);
    }

    if ($user_id == 0) {
        return $defaultVal;
    }

    if (array_key_exists('display', $atts)) {
        $key = $atts['display'];

        $val = get_user_meta($user_id, $key, true );

        if ($val == NULL || $val === '') {
            return $defaultVal;
        }

        // return $key;
        return htmlspecialchars($val);
    }

    return $defaultVal;
}


function user_meta_conditional_shortcode($atts, $content = "") {

    $show = true;
    $admin = current_user_can( 'manage_options' );
    $user_id = get_page_user_id();
    // var_dump($atts);
    // var_dump($atts['usermeta']);

    if (array_key_exists('usermeta', $atts)) {
        $meta = json_decode($atts['usermeta']);
        foreach ($meta as $key => $value) {
            if (!$user_id) {
                $show = false;
                break;
            }

            $includecount = 0;
            $excludecount = 0;
            $includes = false;
            $excludes = false;

            $meta_key = $key;
            $union = true;
            if (substr($meta_key, 0, 1) == "|") {
                $union = false;
                $meta_key = substr($meta_key, 1);
            }

            $meta_val = get_user_meta($user_id, $meta_key, true );
            // var_dump($meta_val);
            // var_dump($meta_val);
            foreach (explode(";", $value) as $key => $splitValue) {
                if (substr($splitValue, 0, 1) == "!") {
                    $splitValue = substr($splitValue, 1);
                    $excludecount += 1;
                    if ($meta_val === $splitValue) {
                        $excludes = true;
                    }

                } else { 
                    $includecount += 1;
                    if ($meta_val === $splitValue) {
                        $includes = true;
                    }
                }
            }

            // var_dump($includes, $excludes, $includecount, $excludecount);

            $result = !((!$includes && $includecount > 0) || ($excludes && $excludecount > 0));
            // var_dump($result, $union);

            if ($union && !$result) {
                $show = false;
            } else if (!$union && !$show) {
                $show = $result;
            }
        }

    }

    $att_prefix = "meta_";
    $not_att_prefix = "notmeta_";
    foreach ($atts as $key => $value) {
        if (substr( $key, 0, strlen($att_prefix) ) === $att_prefix) {

            if (!$user_id) {
                $show = false;
                break;
            }

            $meta_key = substr($key, strlen($att_prefix));
            $meta_val = get_user_meta($user_id, $meta_key, true );
            // var_dump($meta_val);
            if ($meta_val !== $value) {
                $show = false;
            }

        }
        if (substr( $key, 0, strlen($not_att_prefix) ) === $not_att_prefix) {
            if (!$user_id) {
                break;
            }

            $meta_key = substr($key, strlen($not_att_prefix));
            $meta_val = get_user_meta($user_id, $meta_key, true );
            
            var_dump($meta_val);
            var_dump($meta_key);
            // var_dump($meta_val);
            if ($meta_val === $value) {
                $show = false;
            }

        }
    }

    if (
        array_key_exists('authenticated', $atts) &&
        (
            (strtolower($atts['authenticated']) === 'false' && is_user_logged_in()) ||
            (strtolower($atts['authenticated']) === 'true' && !is_user_logged_in())
        )
    ) {
        $show = false;
    }

    if (
        array_key_exists('admin', $atts) &&
        (
            (strtolower($atts['admin']) === 'false' && $admin) ||
            (strtolower($atts['admin']) === 'true' && !$admin)
        )
    ) {
        $show = false;
    }

    if (array_key_exists('rename', $atts)) {
        $result = count_users(); 
        for ($i = 1; $i <= $result['total_users']; $i++) {

            $meta_val = get_user_meta($i, "Team Role", true );
            update_user_meta($i, "TeamRole", $meta_val);

            $meta_val = get_user_meta($i, "School", true );
            update_user_meta($i, "School", $meta_val);
        }
    }
    
    if (array_key_exists('not_before', $atts)) {
        $not_before = strtotime($atts['not_before']);
        // return time() . "|" .  $not_before;
        if ($not_before > time()) {
            $show = false;
        }
    }
    if (array_key_exists('not_after', $atts)) {
        $not_after = strtotime($atts['not_after']);
        if ($not_after < time()) {
            $show = false;
        }
    }

    if ($show) { 
        return do_shortcode($content);
    } else {
        return null;
    }

    // $atts = shortcode_atts( array(
    //     'foo' => 'no foo',
    //     'baz' => 'default baz'
    // ), $atts, 'bartag' );
    // return "content = $content {$atts['foo']}";
}

?>