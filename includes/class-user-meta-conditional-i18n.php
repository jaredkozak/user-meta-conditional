<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://kozan.dev
 * @since      1.0.0
 *
 * @package    User_Meta_Conditional
 * @subpackage User_Meta_Conditional/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    User_Meta_Conditional
 * @subpackage User_Meta_Conditional/includes
 * @author     Jared Kozak <jared@kozan.dev>
 */
class User_Meta_Conditional_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'user-meta-conditional',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
