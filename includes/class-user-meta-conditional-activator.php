<?php

/**
 * Fired during plugin activation
 *
 * @link       https://kozan.dev
 * @since      1.0.0
 *
 * @package    User_Meta_Conditional
 * @subpackage User_Meta_Conditional/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    User_Meta_Conditional
 * @subpackage User_Meta_Conditional/includes
 * @author     Jared Kozak <jared@kozan.dev>
 */
class User_Meta_Conditional_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
