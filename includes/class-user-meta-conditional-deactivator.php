<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://kozan.dev
 * @since      1.0.0
 *
 * @package    User_Meta_Conditional
 * @subpackage User_Meta_Conditional/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    User_Meta_Conditional
 * @subpackage User_Meta_Conditional/includes
 * @author     Jared Kozak <jared@kozan.dev>
 */
class User_Meta_Conditional_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
